//
//  ViewController.swift
//  MixpanelDemo
//
//  Created by Jumpei Katayama on 11/10/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let mixpanel = Mixpanel.sharedInstance()
        mixpanel.track(
            "Plan selected",
            properties: ["Gender": "Female", "Plan": "Premium"]
        )
        
    
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

