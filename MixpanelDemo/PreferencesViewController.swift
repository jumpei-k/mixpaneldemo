//
//  PreferencesViewController.swift
//  MixpanelDemo
//
//  Created by Jumpei Katayama on 11/10/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

enum UserType: Int {
    case Anonymous
    case Free
    case Paid
}

class PreferencesViewController: UIViewController {
    
    private let userType = ["Anonymous", "Free", "Paid"]

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PreferencesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "User Type"
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userType.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let selectedColor = UIColor(red: 1 / 255, green: 15 / 255, blue: 25 / 255, alpha: 1.0)
        let selectedColor = UIColor.blueColor()
        let unselectedColor = UIColor(red: 79 / 255, green: 93 / 255, blue: 102 / 255, alpha: 1.0)
        
        
//        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        var cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell?
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "cell")
        }
        cell?.selectionStyle = .None
        //current usertype
        var currentUserType: UserType
        let userDefaultUserType = NSUserDefaults.standardUserDefaults().objectForKey("userType")
        if let udUserType = userDefaultUserType {
            currentUserType = UserType(rawValue: udUserType as! Int)!
        } else {
            NSUserDefaults.standardUserDefaults().setInteger(UserType.Anonymous.rawValue, forKey: "userType")
            currentUserType = .Anonymous
        }
        
        if currentUserType.rawValue == indexPath.row {
            cell!.accessoryType = .Checkmark// Check Mark doesn't show up!!
            cell!.textLabel?.textColor = selectedColor

        } else {
            cell!.accessoryType = .None
            cell!.textLabel?.textColor = unselectedColor

        }
        
        cell!.textLabel?.text = userType[indexPath.row]
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let userTypeValue = UserType(rawValue: indexPath.row)!
        NSUserDefaults.standardUserDefaults().setInteger(userTypeValue.rawValue, forKey: "userType")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        tableView.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: .None)
        tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
