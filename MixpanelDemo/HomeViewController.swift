//
//  HomeViewController.swift
//  MixpanelDemo
//
//  Created by Jumpei Katayama on 11/10/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

struct JobInterval {
    static let uploadPhoto: UInt32 = 3
}

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let userDefaultUserType = NSUserDefaults.standardUserDefaults().objectForKey("userType")
//        if let udUserType = userDefaultUserType {
//            currentUserType = UserType(rawValue: udUserType as! Int)!
//        } else {
//            NSUserDefaults.standardUserDefaults().setInteger(UserType.Anonymous.rawValue, forKey: "userType")
//            currentUserType = .Anonymous
//        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func timingEvent(sender: AnyObject) {
        let mixpanel = Mixpanel.sharedInstance()
        mixpanel.timeEvent("Image Upload")
        
        self.uploadImageTask({ (result) -> Void in
            mixpanel.track("Image Upload")
            print("Complete uploading image!")
        })
    }

    
    func uploadImageTask(completion: (result: String) -> ()) {
        print("uploading image")
        sleep(JobInterval.uploadPhoto)
        completion(result: "upload success")
    }

    
    @IBAction func unwindToMainContent(segue:UIStoryboardSegue) {
        print("Back to unwindToMainContent")
        if let source = segue.sourceViewController as? PreferencesViewController {
            // Update user type if it's changed
            let mixpanel = Mixpanel.sharedInstance()
            let userDefaultUserType = NSUserDefaults.standardUserDefaults().objectForKey("userType") as! Int
            let userType = UserType(rawValue: userDefaultUserType)
            mixpanel.registerSuperProperties(["User Type": String(userType!)])

        }
    }
}
